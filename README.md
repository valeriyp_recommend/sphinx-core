# README #

To properly manage this documentaion you need to have PIP and GIT installed on you machine.

Installation:

1. Create spinx directory somewhere on your computer, and go there using your command line.
2. Clone "git clone https://valeriyp_recommend@bitbucket.org/valeriyp_recommend/sphinx-core.git ."
3. Go to source folder ("cd source")
4. Run "git clone https://valeriyp_recommend@bitbucket.org/valeriyp_recommend/sphinx-markup.git ."
5. Install Sphinx Bootstrap Theme (https://pypi.python.org/pypi/sphinx-bootstrap-theme/)

To compile changes in markup files run "make html" from folder, containing "make.bat" file.